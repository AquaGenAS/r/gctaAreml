
## Running on Orion. 
Use rstudio-4.0.2 and load the package like this:
```R
library(gctaAreml, lib.loc = "/net/10.222.0.31/home01/tikn/R/library-4.0")
```
## Local installation

### R dependency package from github
```R
devtools::install_github("luansheng/visPedigree") ## Install devtools if you do not have it. 
```
### R Dependencies installed automatically
nadiv and plinkFile

**Install GctaAreml**
```R
library(devtools)
install_gitlab("AquaGenAS/r/gctaAreml/")
```
## Example
 ```R
 # pedigree file contain column: id, sire, dam
 Pedigree_downloaded <- read_delim("~/Downloads/Pedigree_downloaded_201014_123020.txt",
                                  "\t", escape_double = FALSE, trim_ws = TRUE)


A <- gctaAreml::makeA_ped(Pedigree_downloaded) ## Save the A matrix in an R object. 
temp <- tempfile() ## Set up a temp file to save the A matrix
gctaAreml::save_GRM(GRM = temp, A = A)
run_gcta(gcta_path = "/mnt/users/tikn/bioinf_tools/gcta/gcta_1.93.2beta/gcta64",
        GRM = "temp", 
        pheno = "pheno.txt", 
        out = "temp2")
```
